import {Route, Switch} from "react-router-dom";
import Layout from "./componens/UI/Layout/Layout";
import Main from "./containers/Main/Main";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import UserPhotos from "./containers/UserPhotos/UserPhotos";
import AddPhoto from "./containers/AddPhoto/AddPhoto";

const App = () => {
	return (
		<Layout>
			<Switch>
				<Route path="/" exact component={Main}/>
				<Route path="/register" component={Register}/>
				<Route path="/login" component={Login}/>
				<Route path="/photos/users/:id" component={UserPhotos}/>
				<Route path="/add" component={AddPhoto}/>
			</Switch>
		</Layout>
	);
};

export default App;
