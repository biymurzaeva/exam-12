import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import {fetchPhotos} from "../../store/actions/photosActions";
import Photo from "../../componens/Photo/Photo";

const useStyles = makeStyles(theme => ({
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8),
	},
}));

const Main = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const photos = useSelector(state => state.photos.photos);
	const loading = useSelector(state => state.photos.fetchPhotosLoading);
	const error = useSelector(state => state.photos.fetchPhotosError);

	useEffect(() => {
		dispatch(fetchPhotos());
	}, [dispatch]);

	return (
		<Container className={classes.cardGrid} maxWidth="md">
			<Grid container spacing={4}>
			{loading ?
				<Grid container justifyContent="center" alignItems="center">
					<Grid item>
						<CircularProgress/>
					</Grid>
				</Grid> : error ? <Typography variant="h6">{error.error}</Typography> :
				photos.map(photo => (
					<Photo
						key={photo._id}
						user={photo.user}
						title={photo.title}
						image={photo.image}
					/>
				))
			}
			</Grid>
		</Container>
	);
};

export default Main;