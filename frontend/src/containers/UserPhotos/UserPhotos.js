import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {deletePhoto, fetchUserPhotos} from "../../store/actions/photosActions";
import {Button, CircularProgress, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import Photo from "../../componens/Photo/Photo";
import {fetchUser} from "../../store/actions/usersActions";

const useStyles = makeStyles(theme => ({
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8),
	},
}));

const UserPhotos = ({match}) => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const photos = useSelector(state => state.photos.userPhotos);
	const loading = useSelector(state => state.photos.fetchUserPhotosLoading);
	const error = useSelector(state => state.photos.fetchUserPhotosError);
	const author = useSelector(state => state.users.photoUser);
	const user = useSelector(state => state.users.user);

	useEffect(() => {
		dispatch(fetchUserPhotos(match.params.id));
		dispatch(fetchUser(match.params.id));
	}, [dispatch, match.params.id]);

	let addBtn = <></>;

	if (user && author) {
		if (user._id === author._id) {
			addBtn = <Button component={Link} to={'/add'}>Add new photo</Button>
		}
	}

	return (
		<Container className={classes.cardGrid} maxWidth="md">
			<Grid container spacing={4}>
				{loading ?
					<Grid container justifyContent="center" alignItems="center">
						<Grid item>
							<CircularProgress/>
						</Grid>
					</Grid> : error ? <Typography variant="h6">{error.error}</Typography> :
					<>
						<Grid container justifyContent="space-between">
							<Grid item>
								{author && <Typography component="h1" variant="h6">{author.displayName}'s gallery</Typography>}
							</Grid>
							<Grid item>
								{addBtn}
							</Grid>
						</Grid>
						{((user && author) && user._id === author._id) ? photos.map(photo => (
							<Photo
								key={photo._id}
								id={photo._id}
								user={photo.user}
								title={photo.title}
								image={photo.image}
								deleteHandler={() => dispatch(deletePhoto(photo._id))}
							/>
						)) : photos.map(photo => (
							<Photo
								key={photo._id}
								id={photo._id}
								user={photo.user}
								title={photo.title}
								image={photo.image}
							/>
						))}
					</>
				}
			</Grid>
		</Container>
	);
};

export default UserPhotos;