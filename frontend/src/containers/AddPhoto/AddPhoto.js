import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid, makeStyles, TextField} from "@material-ui/core";
import FormElement from "../../componens/UI/Form/FormElement";
import ButtonWithProgress from "../../componens/UI/ButtonWithProgress/ButtonWithProgress";
import {addPhoto, clearPhotoError} from "../../store/actions/photosActions";
import {Redirect} from "react-router-dom";

const useStyles = makeStyles(theme => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		marginTop: theme.spacing(1),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

const AddPhoto = () => {
	const classes = useStyles();
	const dispatch = useDispatch();
	const loading = useSelector(state => state.photos.addLoading)
	const error = useSelector(state => state.photos.addError);
	const user = useSelector(state => state.users.user);

	const [photo, setPhoto] = useState({
		title: '',
		image: null,
	});

	useEffect(() => {
		return () => {
			dispatch(clearPhotoError());
		};
	}, [dispatch]);

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setPhoto(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setPhoto(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();

		Object.keys(photo).forEach(key => {
			formData.append(key, photo[key]);
		});

		dispatch(addPhoto(formData));
	};

	return user ?
		<Container component="section" maxWidth="sm">
			<div className={classes.paper}>
				<Grid
					component="form"
					container
					className={classes.form}
					onSubmit={submitFormHandler}
					spacing={2}
					noValidate
				>
					<FormElement
						type="text"
						required
						label="Title"
						name="title"
						value={photo.title}
						onChange={inputChangeHandler}
						error={getFieldError('title')}
					/>

					<Grid item xs>
						<TextField
							type="file"
							name="image"
							required
							onChange={fileChangeHandler}
							error={Boolean(getFieldError('image'))}
							helperText={getFieldError('image')}
						/>
					</Grid>

					<Grid item xs={12}>
						<ButtonWithProgress
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							className={classes.submit}
							loading={loading}
							disabled={loading}
						>
							Create
						</ButtonWithProgress>
					</Grid>
				</Grid>
			</div>
		</Container> : <Redirect to={'/register'}/>
};

export default AddPhoto;