import {combineReducers} from "redux";
import {configureStore} from "@reduxjs/toolkit";
import createSagaMiddleware from 'redux-saga';
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import axiosApi from "../axiosApi";
import {rootSagas} from "./rootSagas";
import usersSlice, {initialState} from "./slices/usersSlices";
import photosSlices from "./slices/photosSlices";

const rootReducer = combineReducers({
	'users': usersSlice.reducer,
	'photos': photosSlices.reducer,
});

const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
	sagaMiddleware,
];

const store = configureStore({
	reducer: rootReducer,
	middleware,
	devtools: true,
	preloadedState: persistedState,
});

sagaMiddleware.run(rootSagas);

store.subscribe(() => {
	saveToLocalStorage({
		users: {
			...initialState,
			user: store.getState().users.user
		},
	});
});

axiosApi.interceptors.request.use(config => {
	try {
		config.headers['Authorization'] = store.getState().users.user.token
	} catch (e) {}
	return config;
});

axiosApi.interceptors.response.use(res => res, e => {
	if (!e.response) {
		e.response = {data: {global: 'No Internet'}};
	}

	throw e;
});

export default store;