import {call, put, takeEvery} from "redux-saga/effects";
import {
	addPhoto,
	addPhotoFailure,
	addPhotoSuccess, deletePhoto, deletePhotoFailure, deletePhotoSuccess,
	fetchPhotos,
	fetchPhotosFailure,
	fetchPhotosSuccess,
	fetchUserPhotos,
	fetchUserPhotosFailure,
	fetchUserPhotosSuccess
} from "../actions/photosActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* fetchPhotosSaga() {
	try {
		const response = yield axiosApi.get('/photos');
		yield put(fetchPhotosSuccess(response.data));
	} catch (error) {
		yield put(fetchPhotosFailure(error.response.data.error));
	}
}

export function* fetchUserPhotosSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/photos/${id}`);
		yield put(fetchUserPhotosSuccess(response.data));
	} catch (error) {
		yield put(fetchUserPhotosFailure(error.response.data.error));
	}
}

export function* addPhotoSaga({payload: data}) {
	try {
		yield axiosApi.post('/photos', data);
		yield put(addPhotoSuccess());
		yield call(historyPush('/'));
		toast.success('Photo added');
	} catch (error) {
		yield put(addPhotoFailure(error.response.data));
	}
}

export function* deletePhotoSaga({payload: id}) {
	try {
		yield axiosApi.delete(`/photos/${id}`);
		yield put(deletePhotoSuccess(id));
		toast.success('Photo deleted');
	} catch (error) {
		yield put(deletePhotoFailure());
		toast.error(error.response.data.error);
	}
}

const photosSaga = [
	takeEvery(fetchPhotos, fetchPhotosSaga),
	takeEvery(fetchUserPhotos, fetchUserPhotosSaga),
	takeEvery(addPhoto, addPhotoSaga),
	takeEvery(deletePhoto, deletePhotoSaga),
];

export default photosSaga;