import {takeEvery, put, call} from "redux-saga/effects";
import {
	registerUser,
	registerUserFailure,
	registerUserSuccess,
	logoutUser,
	loginUserSuccess,
	loginUserFailure,
	loginUser,
	googleLogin, fetchUserSuccess, fetchUserFailure, fetchUser,
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* registerUserSaga ({payload: userData}) {
	try {
		const response = yield axiosApi.post('/users', userData);
		yield put(registerUserSuccess(response.data));
		yield call(historyPush('/'));
		toast.success('Registration successful');
	} catch (error) {
		yield put(registerUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

export function* logoutUserSaga () {
	try {
		yield call(historyPush('/'));
		yield axiosApi.delete('/users/sessions');
		yield call(historyPush('/'));
	} catch (error) {
		toast.error('Try again');
	}
}

export function* loginUserSaga({payload: userData}) {
	try {
		const response = yield axiosApi.post('/users/sessions', userData);
		yield put(loginUserSuccess(response.data.user));
		yield call(historyPush('/'));
		toast.success('Login successful');
	} catch (error) {
		yield put(loginUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

export function* googleLoginSaga({payload: googleData}) {
	try {
		const response = yield axiosApi.post('/users/googleLogin',
			{
				tokenId: googleData.tokenId,
				googleId: googleData.googleId,
			});
		yield put(loginUserSuccess(response.data.user));
		yield call(historyPush('/'));
		toast.success('Login successful');
	} catch (error) {
		yield put(loginUserFailure(error.response.data));
		toast.error(error.response.data.global);
	}
}

export function* fetchUserSaga({payload: id}) {
	try {
		const response = yield axiosApi.get(`/users/${id}`);
		yield put(fetchUserSuccess(response.data));
	} catch (error) {
		yield put(fetchUserFailure(error));
	}
}

const usersSaga = [
	takeEvery(registerUser, registerUserSaga),
	takeEvery(loginUser, loginUserSaga),
	takeEvery(logoutUser, logoutUserSaga),
	takeEvery(googleLogin, googleLoginSaga),
	takeEvery(fetchUser, fetchUserSaga),
];

export default usersSaga;
