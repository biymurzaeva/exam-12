import {createSlice} from "@reduxjs/toolkit";

const name = 'photo';

const photosSlices = createSlice({
	name,
	initialState: {
		photos: [],
		userPhotos: [],
		fetchPhotosLoading: false,
		fetchPhotosError: null,
		fetchUserPhotosLoading: false,
		fetchUserPhotosError: null,
		addLoading: false,
		addError: null,
		deletePhotoLoading: false,
	},
	reducers: {
		fetchPhotos(state) {
			state.fetchPhotosLoading = true;
		},
		fetchPhotosSuccess(state, action) {
			state.fetchPhotosLoading = false;
			state.photos = action.payload;
		},
		fetchPhotosFailure(state, action) {
			state.fetchPhotosLoading = false;
			state.fetchPhotosError = action.payload;
		},
		fetchUserPhotos(state) {
			state.fetchUserPhotosLoading = true;
		},
		fetchUserPhotosSuccess(state, action) {
			state.fetchUserPhotosLoading = false;
			state.userPhotos = action.payload;
		},
		fetchUserPhotosFailure(state, action) {
			state.fetchUserPhotosLoading = false;
			state.fetchUserPhotosError = action.payload;
		},
		addPhoto(state) {
			state.addLoading = true;
		},
		addPhotoSuccess(state) {
			state.addLoading = false;
		},
		addPhotoFailure(state, action) {
			state.addLoading = false;
			state.addError = action.payload;
		},
		deletePhoto(state) {
			state.deletePhotoLoading = true;
		},
		deletePhotoSuccess(state, action) {
			state.deletePhotoLoading = false;
			state.userPhotos = state.userPhotos.filter(f => f._id !== action.payload);
		},
		deletePhotoFailure(state) {
			state.deletePhotoLoading = false;
		},
		clearPhotoError(state) {
			state.addError = null;
		},
	}
});

export default photosSlices;