const {createSlice} = require("@reduxjs/toolkit");

export const initialState = {
	user: null,
	loginLoading: false,
	loginError: null,
	fetchUserLoading: false,
	fetchUserError: null,
	photoUser: null,
};

const name = 'users';

const usersSlice = createSlice({
	name,
	initialState,
	reducers: {
		registerUser(state) {
			state.registerLoading = true;
		},
		registerUserSuccess(state, {payload: userData}) {
			state.user = userData;
			state.registerLoading = false;
			state.registerError = null;
		},
		registerUserFailure(state, action) {
			state.registerLoading = false;
			state.registerError = action.payload;
		},
		googleLogin() {},
		logoutUser(state) {
			state.user = null;
		},
		loginUser(state) {
			state.loginLoading = true
		},
		loginUserSuccess(state, action) {
			state.loginLoading = false;
			state.loginError = null;
			state.user = action.payload;
		},
		loginUserFailure(state, action) {
			state.loginLoading = false;
			state.loginError = action.payload;
		},
		clearErrorUser(state) {
			state.loginError = null;
			state.registerError = null;
		},
		fetchUser(state) {
			state.fetchUserLoading = true;
		},
		fetchUserSuccess(state, action) {
			state.fetchUserLoading = false;
			state.photoUser = action.payload;
		},
		fetchUserFailure(state, action) {
			state.fetchUserLoading = false;
			state.fetchUserError = action.payload;
		},
	}
});

export default usersSlice;