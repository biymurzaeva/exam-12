import usersSlice from "../slices/usersSlices";

export const {
	registerUser,
	registerUserFailure,
	registerUserSuccess,
	loginUser,
	loginUserSuccess,
	loginUserFailure,
	logoutUser,
	clearErrorUser,
	googleLogin,
	fetchUser,
	fetchUserSuccess,
	fetchUserFailure,
} = usersSlice.actions;
