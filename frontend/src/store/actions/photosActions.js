import photosSlices from "../slices/photosSlices";

export const {
	fetchPhotos,
	fetchPhotosSuccess,
	fetchPhotosFailure,
	fetchUserPhotos,
	fetchUserPhotosSuccess,
	fetchUserPhotosFailure,
	addPhoto,
	addPhotoSuccess,
	addPhotoFailure,
	deletePhoto,
	deletePhotoSuccess,
	deletePhotoFailure,
	clearPhotoError,
} = photosSlices.actions;