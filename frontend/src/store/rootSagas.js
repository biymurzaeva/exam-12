import {all} from 'redux-saga/effects';
import registerUserSaga from "./sagas/usersSagas";
import photosSagas from "./sagas/photosSagas";

export function* rootSagas() {
	yield all([
		...registerUserSaga,
		...photosSagas
	]);
}