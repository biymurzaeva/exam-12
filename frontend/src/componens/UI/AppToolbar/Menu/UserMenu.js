import React from 'react';
import {useDispatch} from "react-redux";
import {Button, Grid, makeStyles} from "@material-ui/core";
import {apiURL} from "../../../../config";
import {logoutUser} from "../../../../store/actions/usersActions";
import defaultAvatar from "../../../../assets/images/default_avatar.png";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
	avatar: {
		width: '35px',
		height: '35px',
		borderRadius: '50%'
	},
});

const UserMenu = ({user}) => {
	const classes = useStyles();
  const dispatch = useDispatch();

	let avatarImg = defaultAvatar;
	const extensions = ['.png', '.jpg', '.jpeg', '.svg', '.gif'];

	if (user.avatar) {
		if (extensions.includes(user.avatar.slice(user.avatar.length - 4))) {
			avatarImg = apiURL + '/' + user.avatar;
		} else {
			avatarImg = user.avatar;
		}
	}

  return (
    <Grid container justifyContent="space-between" alignItems="center">
	    <img src={avatarImg} alt="Avatar" className={classes.avatar}/>
      <Button aria-controls="simple-menu" aria-haspopup="true" component={Link} to={`/photos/users/${user._id}`} color="inherit">
        {user.displayName}!
      </Button>
	    <Button color="inherit" onClick={() => dispatch(logoutUser())}>Logout</Button>
    </Grid>
  );
};

export default UserMenu;