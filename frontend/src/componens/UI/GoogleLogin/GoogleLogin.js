import React from 'react';
import GoogleLoginButton from 'react-google-login';
import {useDispatch} from "react-redux";
import {Button} from "@material-ui/core";
import {FcGoogle} from "react-icons/fc";
import {googleClientId} from "../../../config";
import {googleLogin} from "../../../store/actions/usersActions";

const GoogleLogin = () => {
	const dispatch = useDispatch();

	const handleLogin = response => {
		dispatch(googleLogin(response));
	};

	return (
		<GoogleLoginButton
			clientId={googleClientId}
			render={props => (
				<Button
					fullWidth
					variant="outlined"
					color="primary"
					onClick={props.onClick}
					startIcon={<FcGoogle/>}
				>
					Log in with Google
				</Button>
			)}
			onSuccess={handleLogin}
			cookiePolicy={'single_host_origin'}
		/>
	);
};

export default GoogleLogin;