import React, {useState} from 'react';
import {Link} from "react-router-dom";
import {Button, Card, CardActions, CardContent, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import ModalUI from "../UI/ModalUI/ModalUI";

const useStyles = makeStyles(() => ({
	card: {
		height: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	cardMedia: {
		paddingTop: '115.25%',
	},
	cardContent: {
		flexGrow: 1,
	},
	cardAction: {
		display: "flex"
	},
	modalImg: {
		maxWidth: '350px',
	},
	btn: {
		padding: 0,
	},
}));

const Photo = ({user, image, title, deleteHandler}) => {
	const classes = useStyles();
	const [open, setOpen] = useState(false);

	const handleOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	return (
		<>
			{open ?
			<ModalUI open={open} close={handleClose}>
				<div style={{textAlign: 'end'}}>
					<Button onClick={handleClose} className={classes.btn}>x</Button>
				</div>
				<img src={apiURL + '/' + image} alt={title} className={classes.modalImg}/>
			</ModalUI>: <></>}
			<Grid item xs={12} sm={6} md={4}>
				<Card className={classes.card}>
					<CardMedia
						className={classes.cardMedia}
						image={apiURL + '/' + image}
						title="Image title"
						onClick={handleOpen}
					/>
					<CardContent className={classes.cardContent}>
						<Typography color="primary" component="h1" variant="h6">{title}</Typography>
					</CardContent>
					<CardActions className={classes.cardAction}>
						{user.displayName && <>
						By: <Button color="primary" component={Link} to={`photos/users/${user._id}`}>{user.displayName}</Button>
						</>}
						{deleteHandler && <Button onClick={deleteHandler}>Delete</Button>}
					</CardActions>
				</Card>
			</Grid>
		</>
	);
};

export default Photo;