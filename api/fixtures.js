const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");

const run = async () => {
	await mongoose.connect(config.db.url);

	const collections = await mongoose.connection.db.listCollections().toArray();

	for (const coll of collections) {
		await mongoose.connection.db.dropCollection(coll.name);
	}

	const [admin, user, test] = await User.create({
		email: 'admin@gmail.com',
		password: 'admin',
		token: nanoid(),
		role: 'admin',
		displayName: 'Admin',
	}, {
		email: 'user@gmail.com',
		password: 'user',
		token: nanoid(),
		role: 'user',
		displayName: 'User',
	}, {
		email: 'test@gmail.com',
		password: 'test',
		token: nanoid(),
		role: 'user',
		displayName: 'Test',
	});

	await Photo.create({
		user: admin,
		title: 'Flower',
		image: 'fixtures/flower.jpg'
	}, {
		user: admin,
		title: 'Nature',
		image: 'fixtures/photo1.jpg'
	}, {
		user: user,
		title: 'Nature',
		image: 'fixtures/photo2.jpg'
	}, {
		user: test,
		title: 'City',
		image: 'fixtures/photo3.jpg'
	});

	await mongoose.connection.close();
};

run().catch(console.error);