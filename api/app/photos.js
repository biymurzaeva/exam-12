const express = require('express');
const path = require("path");
const {nanoid} = require("nanoid");
const multer = require("multer");
const Photo = require('../models/Photo');
const auth = require("../middleware/auth");
const config = require("../config");
const User = require("../models/User");

const router = express.Router();

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

router.get('/', async (req, res) => {
	try {
		const response = await Photo.find().populate('user', 'displayName');

		if (response.length === 0) {
			return res.status(404).send({error: 'Photos not found'});
		}

		res.send(response);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.get('/:id', async (req, res) => {
	try {
		const user = await User.findById(req.params.id);

		if (!user) {
			return res.status(404).send({error: 'User not found'});
		}

		const photos = await Photo.find({user: req.params.id});

		if (!photos) {
			return res.status(404).send({error: 'Photos not found'});
		}

		res.send(photos);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', auth, upload.single('image'), async (req, res) => {
	try {
		const photoData = {
			user: req.user._id,
			title: req.body.title,
		};

		if (req.file) {
			photoData.image = 'uploads/' + req.file.filename;
		}

		const photo = new Photo(photoData);
		await photo.save();
		res.send(photo);
	} catch (error) {
		res.status(400).send(error);
	}
});

router.delete('/:id', auth, async (req, res) => {
	try {
		const photo = await Photo.findById(req.params.id);

		if (!photo) {
			return res.status(404).send({error: 'Photo not found'});
		}

		if (req.user._id.toString() !== photo.user._id.toString()) {
			return res.status(403).send({error: 'Permissions denied'});
		}

		await Photo.findByIdAndRemove(req.params.id);
		res.send({message: 'Photo removed'});
	} catch (error) {
		res.sendStatus(500);
	}
});

module.exports = router;